# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 12:45:16 2020

@author: JP Molina
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import r2_score
from sklearn.svm import SVR
from math import sqrt
from sklearn.metrics import mean_squared_error, mean_absolute_error
import datetime
plt.style.use('fivethirtyeight')


#Adquirir todos los archivos

confirmados = pd.read_csv('confirmados.csv', sep=',')
recuperados = pd.read_csv('recuperados.csv', sep=',')
muertos = pd.read_csv('muertos.csv', sep=',')
datos_completos = pd.read_csv('datos_completos.csv', sep=',')

#Limpieza de Datos

confirmados['Fecha'] = pd.to_datetime(confirmados['Fecha'])
recuperados['Fecha'] = pd.to_datetime(recuperados['Fecha'])
muertos['Fecha'] = pd.to_datetime(muertos['Fecha'])
datos_completos['Fecha'] = pd.to_datetime(datos_completos['Fecha'])
'''
print('Casos Confirmados')
print(confirmados.describe())
print('\n')
print('Casos Recuperados')
print(recuperados.describe())
print('\n')
print('Pacientes Muertos')
print(muertos.describe())
print('\n')
print('Contagios diarios')
print(datos_completos['Contagios_Nuevos_Universidad'].describe())
'''

#Regresion Polinomial
fechas_confirmados = recuperados['Fecha']

#Resumen de los datos
casos_totales = 142475
muertes_totales = 8818
total_recuperados = 115718
total_activos = casos_totales - muertes_totales - total_recuperados
tasa_de_mortalidad = muertes_totales / casos_totales
indice_de_recuperacion = total_recuperados / casos_totales

datos_finales = pd.DataFrame(datos_completos)

dias_desde_3_10 = np.array([i for i in range(len(fechas_confirmados))]).reshape(-1, 1)
dias_en_el_futuro = 20
pronostico = np.array([i for i in range(len(fechas_confirmados) + dias_en_el_futuro)]).reshape(-1, 1)
dias_ajustados = pronostico[:-20]


inicio = '3/31/2020'
fecha_inicio = datetime.datetime.strptime(inicio, '%m/%d/%Y')
fechas_de_prediccion = []
casos_Bolivia = recuperados['Bolivia_Universidad']
casos_Bolivia = np.array(casos_Bolivia).reshape(-1, 1)

for i in range(len(pronostico)):
    fechas_de_prediccion.append((fecha_inicio + datetime.timedelta(days=i)).strftime('%m/%d/%Y'))

x_entrenamiento_confirmado, x_prueba_confirmado, y_entrenamiento_confirmado, y_prueba_confirmado = train_test_split(dias_desde_3_10, casos_Bolivia, test_size=0.35, shuffle=False)

#prueba
polynomial_features= PolynomialFeatures(degree=3)
x_poly = polynomial_features.fit_transform(dias_desde_3_10)

modeloLinear = LinearRegression()
modeloLinear.fit(x_poly, casos_Bolivia)
y_pred = modeloLinear.predict(x_poly)
'''
print('MAE:', mean_absolute_error(casos_Bolivia, y_pred))
print('RMSE:', sqrt(mean_squared_error(casos_Bolivia, y_pred)))
print('R^2', r2_score(casos_Bolivia, y_pred))
'''
lineary_pred = y_pred.reshape(1,-1)[0]
'''
poly_df = pd.DataFrame({'Fecha': fechas_de_prediccion[-25:], 'Prediccion de Numero de Casos': np.round(lineary_pred[-25:])})
print(poly_df)
'''

svm_confirmed = SVR(shrinking=True, kernel='poly',gamma=0.01, epsilon=1, degree=3, C=0.1)
svm_confirmed.fit(x_entrenamiento_confirmado, y_entrenamiento_confirmado.ravel())
svm_pred = svm_confirmed.predict(pronostico)
svm_test_pred = svm_confirmed.predict(x_prueba_confirmado)

print('MAE:', mean_absolute_error(svm_test_pred, y_prueba_confirmado))
print('RMSE:',sqrt(mean_squared_error(svm_test_pred, y_prueba_confirmado)))
print('R^2', r2_score(svm_test_pred, y_prueba_confirmado))

lineary_pred = y_prueba_confirmado.reshape(1,-1)[0]

poly_df = pd.DataFrame({'Fecha': fechas_de_prediccion[-25:], 'Prediccion de Numero de Casos': np.round(lineary_pred[-25:])})
print(poly_df)

def plot_predictions(x, y, pred, algo_name, color):
    plt.figure(figsize=(16, 9))
    plt.plot(x, y)
    plt.plot(pronostico, pred, linestyle='dashed', color=color)
    plt.title('Numero de Casos Recuperados por COVID-19 en el tiempo', size=30)
    plt.xlabel('Fecha desde el 31/03/2020', size=30)
    plt.ylabel('Numero de Recuperados', size=30)
    plt.legend(['Recuperados Confirmadas', algo_name], prop={'size': 20})
    plt.xticks(size=20)
    plt.yticks(size=20)
    plt.show()
plot_predictions(dias_ajustados, casos_Bolivia, svm_pred, 'Predicciones del Modelo SVM', 'red')