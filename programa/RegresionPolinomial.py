# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 19:04:10 2020

@author: JP Molina
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.colors as mcolors
import seaborn as sns
import random
import math
import time
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import r2_score
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, mean_absolute_error
import datetime
import operator 
plt.style.use('fivethirtyeight')


#Adquirir todos los archivos

confirmados = pd.read_csv('confirmados.csv', sep=',')
recuperados = pd.read_csv('recuperados.csv', sep=',')
muertos = pd.read_csv('muertos.csv', sep=',')
datos_completos = pd.read_csv('datos_completos.csv', sep=',')

#Limpieza de Datos

confirmados['Fecha'] = pd.to_datetime(confirmados['Fecha'])
recuperados['Fecha'] = pd.to_datetime(recuperados['Fecha'])
muertos['Fecha'] = pd.to_datetime(muertos['Fecha'])
datos_completos['Fecha'] = pd.to_datetime(datos_completos['Fecha'])
'''
print('Casos Confirmados')
print(confirmados.describe())
print('\n')
print('Casos Recuperados')
print(recuperados.describe())
print('\n')
print('Pacientes Muertos')
print(muertos.describe())
print('\n')
print('Contagios diarios')
print(datos_completos['Contagios_Nuevos_Universidad'].describe())
'''

#Regresion Polinomial
fechas_confirmados = confirmados['Fecha']

#Resumen de los datos
casos_totales = 142475
muertes_totales = 8818
total_recuperados = 115718
total_activos = casos_totales - muertes_totales - total_recuperados
tasa_de_mortalidad = muertes_totales / casos_totales
indice_de_recuperacion = total_recuperados / casos_totales

datos_finales = pd.DataFrame(datos_completos)

dias_desde_3_10 = np.array([i for i in range(len(fechas_confirmados))]).reshape(-1, 1)
dias_en_el_futuro = 20
pronostico = np.array([i for i in range(len(fechas_confirmados) + dias_en_el_futuro)]).reshape(-1, 1)
dias_ajustados = pronostico[:-20]


inicio = '3/10/2020'
fecha_inicio = datetime.datetime.strptime(inicio, '%m/%d/%Y')
fechas_de_prediccion = []
casos_Bolivia = confirmados['Bolivia_Universidad']
casos_Bolivia = np.array(casos_Bolivia).reshape(-1, 1)

for i in range(len(pronostico)):
    fechas_de_prediccion.append((fecha_inicio + datetime.timedelta(days=i)).strftime('%m/%d/%Y'))

x_entrenamiento_confirmado, x_prueba_confirmado, y_entrenamiento_confirmado, y_prueba_confirmado = train_test_split(dias_desde_3_10, casos_Bolivia, test_size=0.15, shuffle=False)

#transformar los datos para la Regresion Polinomial
poli = PolynomialFeatures(degree=3)
poli_x_entrenamiento_confirmado = poli.fit_transform(x_entrenamiento_confirmado.reshape(-1, 1))
poli_x_prueba_confirmado = poli.fit_transform(x_prueba_confirmado.reshape(-1, 1))
poli_pronostico = poli.fit_transform(pronostico.reshape(-1, 1))

#Regresion Polinomial
linear_model  = LinearRegression()
linear_model.fit(poli_x_entrenamiento_confirmado, y_entrenamiento_confirmado)
test_linear_pred = linear_model.predict(poli_x_prueba_confirmado)
linear_pred = linear_model.predict(poli_pronostico)


print('MAE:', mean_absolute_error(test_linear_pred, y_prueba_confirmado))
print('MSE:',mean_squared_error(test_linear_pred, y_prueba_confirmado))
print('R^2', r2_score(test_linear_pred, y_prueba_confirmado))
poly_df = pd.DataFrame({'Date': fechas_de_prediccion[-20:], 'Predicted number of Confirmed Cases Worldwide': np.round(linear_pred[-20:])})
poly_df
plt.plot(y_prueba_confirmado)
plt.plot(test_linear_pred)
plt.legend(['Test Data', 'Polynomial Regression Predictions'])

'''
svm_confirmed = SVR(shrinking=True, kernel='poly',gamma=0.01, epsilon=1,degree=5, C=0.1)
svm_confirmed.fit(x_entrenamiento_confirmado, y_entrenamiento_confirmado)
svm_pred = svm_confirmed.predict(pronostico)
svm_test_pred = svm_confirmed.predict(x_prueba_confirmado)
plt.plot(y_prueba_confirmado)
plt.plot(svm_test_pred)
plt.legend(['Test Data', 'SVM Predictions'])
print('MAE:', mean_absolute_error(svm_test_pred, y_prueba_confirmado))
print('MSE:',mean_squared_error(svm_test_pred, y_prueba_confirmado))
'''